using System;

namespace DriverApi.AcceptanceTests.Helpers
{
    public class GlobalTestBase : IDisposable
    {
        public GlobalTestBase()
        {
            Database.EnsureDropped();
            Database.EnsureCreated();
            Database.EnsureSchemaLoaded();
            Database.Seed();
        }

        public void Dispose() {}
    }
}
