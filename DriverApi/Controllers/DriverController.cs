﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using SkyDapperExtensions.Services.Interfaces;
using DriverApi.Services.Interfaces;
using DriverApi.Pagination;
using DriverApi.Models;
using SkyDataTransferModels;
using DriverApi.Exceptions;
using Microsoft.AspNetCore.Authorization;
using DriverApi.Resources;

namespace DriverApi.Controllers
{
    /// <summary>
    /// Controller for Driver
    /// </summary>
    [Route("/drivers")]
    [Authorize]
    public class DriverController : Controller
    {
        private readonly IDriverDataService _DriverService;
        private readonly IAgvanceDatabaseConnectionService _agvanceDatabaseConnectionService;
        private readonly IBuildsPaginationResponses<Driver> _paginationFactoryObject;

        /// <summary>
        /// Instantiates a new instance of the Driver Controller with the necessary services
        /// </summary>
        /// <param name="DriverService"></param>
        /// <param name="agvanceDatabaseConnectionService"></param>
        /// <param name="paginationFactoryObject"></param>
        public DriverController(IDriverDataService DriverService, IAgvanceDatabaseConnectionService agvanceDatabaseConnectionService, IBuildsPaginationResponses<Driver> paginationFactoryObject)
        {
            _agvanceDatabaseConnectionService = agvanceDatabaseConnectionService;
            _DriverService = DriverService;
            _paginationFactoryObject = paginationFactoryObject;
        }
        
        /// <summary>
        /// Returns all Drivers
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="orderByPropertyName"></param>
        /// <param name="orderBySortDirection"></param>
        /// <param name="IsInactive"></param>
        /// <returns>List of Drivers</returns>
        /// <response code="200">returns list of delivery ticket</response>
        /// <response code="400">bad request with a message</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(typeof(List<Driver>), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        [HttpGet]
        public IActionResult Get([FromHeader] string databaseId, 
               [FromQuery] int pageNumber = 1,
               [FromQuery] string orderByPropertyName = "Id",
               [FromQuery] string orderBySortDirection = "ASC",
               [FromQuery] bool? IsInactive = null)
        {
            try
            {
                if (string.IsNullOrEmpty(databaseId))
                {
                    return BadRequest(ValidationMessages.NoDatabaseIdMessage);
                }

                _agvanceDatabaseConnectionService.SetDatabaseId(databaseId);
                _DriverService.SetPageSize(20);
                _DriverService.SetPageNumber(pageNumber);

                SortDirectionEnum sortDirection;

                if (orderBySortDirection == "ASC")
                {
                    sortDirection = SortDirectionEnum.ASC;
                }
                else if (orderBySortDirection == "DESC")
                {
                    sortDirection = SortDirectionEnum.DESC;
                }
                else
                {
                    return StatusCode((int)HttpStatusCode.BadRequest);
                }

                _DriverService.SetOrderBy(new OrderBy { propertyName = orderByPropertyName, sortDirection = sortDirection });

                if (IsInactive != null)
                { 
                    _DriverService.SetInactive((bool)IsInactive);
                }
                var Drivers = _DriverService.FetchList();

                if (Drivers.Any())
                {
                    var pagedDrivers = _paginationFactoryObject.Create(Drivers, pageNumber, Drivers.First().TotalSqlRowCount);

                    return Ok(pagedDrivers);
                }
                return NotFound();
            }
            catch (DriverPageConstructionException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
