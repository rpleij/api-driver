﻿using System;

namespace DriverApi.AcceptanceTests.Helpers
{
    public class TestHostAlreadyCreated : Exception
    {
        public TestHostAlreadyCreated()
            : base("A test host has already been estalished") {}
    }
}