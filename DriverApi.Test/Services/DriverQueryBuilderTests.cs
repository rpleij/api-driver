﻿using DriverApi.Models;
using DriverApi.Services;
using Dapper;
using SkyDataTransferModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DriverApi.Test.Services
{
    public class DriverQueryBuilderTests
    {
        [Fact]
        public void GivenADriverGuidReturnWhereClause()
        {
            // arrange
            var isInactive = true;
            var expectedWhereClause = $"AND agdriver.Inactive = @{nameof(Driver.IsInactive)}";
            var expectedParameters = new DynamicParameters();
            expectedParameters.Add(nameof(Driver.IsInactive), isInactive);
            var queryBuilder = new DriverQueryBuilder();

            // act
            queryBuilder.SetInactive(isInactive);
            var result = queryBuilder.Build();

            // assert
            Assert.Contains(expectedWhereClause, result.Sql);
            Assert.Equal(result.SqlParameters.Get<bool>(nameof(Driver.IsInactive)), isInactive);
        }
        [Fact]
        public void GivenASetPageSizeReturnWhereFetchOnlyClause()
        {
            // arrange
            var pageSize = 20;
            string placeHolder = "FetchNextRowsOnly";
            var expectedWhereClause = $"FETCH NEXT @{placeHolder} ROWS ONLY";
            var expectedParameters = new DynamicParameters();
            expectedParameters.Add(placeHolder, pageSize);
            var queryBuilder = new DriverQueryBuilder();

            // act
            queryBuilder.SetPageSize(20);
            var result = queryBuilder.Build();

            // assert
            Assert.Contains(expectedWhereClause, result.Sql);
            Assert.Equal(result.SqlParameters.Get<int>(placeHolder), pageSize);
        }
        [Fact]
        public void GivenASetPageNumberReturnWhereOffSetRows()
        {
            // arrange
            var pageSize = 20;
            var pageNumber = 2;
            string placeHolder = "OffSet";
            var expectedWhereClause = $"OFFSET @{placeHolder} ROWS";
            var expectedParameters = new DynamicParameters();
            expectedParameters.Add(placeHolder, pageNumber);
            var queryBuilder = new DriverQueryBuilder();

            // act
            queryBuilder.SetPageNumber(2);
            var result = queryBuilder.Build();

            // assert
            Assert.Contains(expectedWhereClause, result.Sql);
            Assert.Equal(result.SqlParameters.Get<int>(placeHolder), (pageNumber - 1) * pageSize);
        }
        [Fact]
        public void GivenASetPageNumberReturnWhereAddOrderBy()
        {
            // arrange
            var tableName = "agdriver";
            var columnName = "fname";
            var sortOrder = "ASC";
            var expectedWhereClause = $"ORDER BY [{tableName}].[{columnName}] {sortOrder}";
            var queryBuilder = new DriverQueryBuilder();

            // act
            queryBuilder.SetOrderBy(new OrderBy() { propertyName = "FirstName", sortDirection = SortDirectionEnum.ASC });
            var result = queryBuilder.Build();

            // assert
            Assert.Contains(expectedWhereClause, result.Sql);
        }
    }
}
