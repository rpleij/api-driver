﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DriverApi.Exceptions
{
    public class DriverPageConstructionException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public DriverPageConstructionException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public DriverPageConstructionException(string message) : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public DriverPageConstructionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
