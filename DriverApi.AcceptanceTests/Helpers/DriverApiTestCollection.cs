using Xunit;

namespace DriverApi.AcceptanceTests.Helpers
{
    [CollectionDefinition("DriverApi")]
    public class apidriverTestCollection : ICollectionFixture<GlobalTestBase>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}