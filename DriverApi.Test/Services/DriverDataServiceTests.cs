﻿using DriverApi.Models;
using DriverApi.Services;
using DriverApi.Services.Interfaces;
using Dapper;
using Moq;
using SkyDapperExtensions.Services.Interfaces;
using SkyDataTransferModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace DriverApi.Test.Services
{
    public class DriverDataServiceTests
    {
        [Fact]
        public void GivenRequestReturnAllDrivers()
        {
            // arrange
            var expectedResult = ExpectedResult();
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();


            var sqlParams = new DynamicParameters();
            var sqlWithParameters = new SqlWithParameters
            {
                Sql = "SELECT TEST FROM TEST",
                SqlParameters = sqlParams
            };

            driverQueryBuilder.Setup(s => s.Build()).Returns(sqlWithParameters);

            connection.Setup(c => c.Query<Driver>(sqlWithParameters.Sql, sqlWithParameters.SqlParameters, null, true, null, null)).Returns(expectedResult);
            connectionService.Setup(c => c.GetConnection()).Returns(connection.Object);
            connectionService.Object.SetDatabaseId("databaseid");
            

            var driverDataService = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            var result = driverDataService.FetchList();

            // assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public async Task GivenRequestReturnAllDriversAsync()
        {
            // arrange
            var expectedResult = ExpectedResult();
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();


            var sqlParams = new DynamicParameters();
            var sqlWithParameters = new SqlWithParameters
            {
                Sql = "SELECT TEST FROM TEST",
                SqlParameters = sqlParams
            };

            driverQueryBuilder.Setup(s => s.Build()).Returns(sqlWithParameters);

            connection.Setup(c => c.QueryAsync<Driver>(sqlWithParameters.Sql, sqlWithParameters.SqlParameters, null, true, null, null)).Returns(async () => expectedResult);
            connectionService.Setup(c => c.GetConnection()).Returns(connection.Object);
            connectionService.Object.SetDatabaseId("databaseid");


            var driverDataService = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            var result = await driverDataService.FetchListAsync();

            // assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void GivenDriverInactive_SetInactiveCallsQueryBuilder()
        {
            // arrange
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();
            var service = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            service.SetInactive(true);

            // assert
            driverQueryBuilder.Verify(x => x.SetInactive(true), Times.Once);
        }

        [Fact]
        public void GivenPageSize_SetPageSizeCallsQueryBuilder()
        {
            // arrange
            var size = 20;
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();
            var service = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            service.SetPageSize(size);

            // assert
            driverQueryBuilder.Verify(x => x.SetPageSize(size), Times.Once);
        }

        [Fact]
        public void GivenPageNumber_SetPageNumberCallsQueryBuilder()
        {
            // arrange
            var number = 3;
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();
            var service = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            service.SetPageNumber(number);

            // assert
            driverQueryBuilder.Verify(x => x.SetPageNumber(number), Times.Once);
        }

        [Fact]
        public void GivenOrderBy_SetOrderByCallsQueryBuilder()
        {
            // arrange
            var orderBy = new OrderBy
            {
                propertyName = nameof(Driver.FirstName),
                sortDirection = SortDirectionEnum.ASC
            };
            var connectionService = new Mock<IAgvanceDatabaseConnectionService>();
            var connection = new Mock<IConnection>();
            var driverQueryBuilder = new Mock<IDriverQueryBuilder>();
            var service = new DriverDataService(driverQueryBuilder.Object, connectionService.Object);

            // act
            service.SetOrderBy(orderBy);

            // assert
            driverQueryBuilder.Verify(x => x.SetOrderBy(orderBy), Times.Once);
        }

        private List<Driver> ExpectedResult()
        {
            return new List<Driver>
            {
                new Driver
                {
                    UniqueId = 8,
                    Id = "EL IZ",
                    FirstName = "Elizabeth",
                    LastName = "Bunton",
                    DriverType = "Applicator Type 1",
                    DivisionType = "All",
                    EmailAddress = "email@email.net",
                    IsInactive = false
                },
                new Driver
                {
                    UniqueId = 4,
                    Id = "Oatwil",
                    FirstName = "William",
                    LastName = "Oates",
                    DriverType = "",
                    DivisionType = "Agronomy",
                    EmailAddress = "woates@agvance.net",
                    IsInactive = false
                },
                new Driver
                {
                    UniqueId = 17,
                    Id = "CM",
                    FirstName = "Craig",
                    LastName = "Craig",
                    DriverType = "",
                    DivisionType = "Agronomy",
                    EmailAddress = "",
                    IsInactive = true
                }
            };
        }
    }
}
