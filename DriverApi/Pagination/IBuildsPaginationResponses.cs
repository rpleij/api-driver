﻿using System.Collections.Generic;

namespace DriverApi.Pagination
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBuildsPaginationResponses<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Driver"></param>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <returns></returns>
        PaginationResponse<T> Create(List<T> Driver, int pageNumber, int numberOfRecords);
    }
}