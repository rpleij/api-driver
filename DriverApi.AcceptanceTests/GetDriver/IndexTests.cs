﻿using DriverApi.AcceptanceTests.Helpers;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DriverApi.Resources;
using Microsoft.AspNetCore.Hosting;
using Xunit;

namespace DriverApi.AcceptanceTests.GetDriver
{
    [Collection("DriverApi")]
    public class IndexTests : TestBase
    {
        public IndexTests()
        {
            // seed data through `Connection`
            EstablishTestHost();
        }

        [Fact(DisplayName = "When given a valid legacy database id and " +
                            "no page number, it responds" +
                            "with a 200 status code and the expected JSON" +
                            "representation of the first page of the " +
                            "paginated list of Drivers")]
        public async Task GivenNoIdGetReturnsListOfDriver()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "valid_token");
            getRequest.Headers.Host = "localhost";
            getRequest.Headers.Add("DatabaseId", DatabaseId);

            // act
            var result = await HttpClient.SendAsync(getRequest);
            var resultBody = await result.Content.ReadAsStringAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var parsedResult = JObject.Parse(resultBody);
            Assert.Equal(16, parsedResult["data"].Children().Count());
            Assert.Equal($"{requestPath}?pageNumber=1", parsedResult["linksMetaData"]["first"]);
            Assert.Equal($"{requestPath}?pageNumber=1", parsedResult["linksMetaData"]["self"]);
            Assert.Equal($"{requestPath}?pageNumber=1", parsedResult["linksMetaData"]["last"]);
            Assert.Equal(null, parsedResult["linksMetaData"]["next"].ToObject<string>());
            Assert.Equal(null, parsedResult["linksMetaData"]["prev"].ToObject<string>());
            Assert.Equal(1, parsedResult["metaResponseData"]["totalPages"]);
        }

        [Fact(DisplayName = "When given a valid legacy database id and " +
                            "a valid page number, it responds" +
                            "with a 200 status code and the expected JSON" +
                            "representation of the first page of the " +
                            "paginated list of Drivers")]
        public async Task GivenPageNumeberGetReturnsListOfDriver()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "drivers?pageNumber=1";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "valid_token");
            getRequest.Headers.Host = "localhost";
            getRequest.Headers.Add("DatabaseId", DatabaseId);

            // act
            var result = await HttpClient.SendAsync(getRequest);
            var resultBody = await result.Content.ReadAsStringAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var parsedResult = JObject.Parse(resultBody);
            Assert.Equal(16, parsedResult["data"].Children().Count());
        }

        [Fact(DisplayName = "When given a valid legacy database id and " +
                            "an invalid page number, it responds with a" +
                            "404 status code")]
        public async Task CallingGetDriversWithInvalidPageNumber()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/?pageNumber=50";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "valid_token");
            getRequest.Headers.Host = "localhost";
            getRequest.Headers.Add("DatabaseId", DatabaseId);

            // act
            var result = await HttpClient.SendAsync(getRequest);

            // assert
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact(DisplayName = "Given no database Id, Request returns 400 Bad request with specific message")]
        public async Task CallingGetDriverWithNoDatabaseId()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "invalid_token");
            getRequest.Headers.Host = "localhost";

            HttpResponseMessage result = null;

            // act
            result = await HttpClient.SendAsync(getRequest);
            
            var responseObject = await result.Content.ReadAsStringAsync();

            // assert
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.Equal(ValidationMessages.NoDatabaseIdMessage, responseObject);
        }

        public async Task CallingGetDriverWithSpecificPageNumber()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/?pageNumber=3";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "valid_token");
            getRequest.Headers.Host = "localhost";
            getRequest.Headers.Add("DatabaseId", DatabaseId);

            // act
            var result = await HttpClient.SendAsync(getRequest);
            var resultBody = await result.Content.ReadAsStringAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var parsedResult = JObject.Parse(resultBody);
            Assert.Equal(20, parsedResult["data"].Children().Count());
            Assert.Equal($"{requestPath}?pageNumber=1", parsedResult["linksMetaData"]["first"]);
            Assert.Equal($"{requestPath}?pageNumber=3", parsedResult["linksMetaData"]["self"]);
            Assert.Equal($"{requestPath}?pageNumber=5", parsedResult["linksMetaData"]["last"]);
            Assert.Equal($"{requestPath}?pageNumber=4", parsedResult["linksMetaData"]["next"].ToObject<string>());
            Assert.Equal($"{requestPath}?pageNumber=2", parsedResult["linksMetaData"]["prev"].ToObject<string>());
            Assert.Equal(5, parsedResult["metaResponseData"]["totalPages"]);
        }

        [Fact(DisplayName = "Given a IsInactive=false, Dont return drivers that are inactive ")]
        public async Task CallGetDriverWithNoVoidedTicketsReturnsNoVoidedTickets()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/?IsInactive=false";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "valid_token");
            getRequest.Headers.Host = "localhost";
            getRequest.Headers.Add("DatabaseId", DatabaseId);

            // act
            var result = await HttpClient.SendAsync(getRequest);
            var resultBody = await result.Content.ReadAsStringAsync();

            // assert
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var parsedResult = JObject.Parse(resultBody);
            Assert.Equal(16, parsedResult["data"].Children().Count());
        }
    }
    [Collection("DriverApi")]
    public class IndexTestsNotAuthorized : TestBase
    {
        public IndexTestsNotAuthorized() : base(new WebHostBuilder().UseStartup<Startup>())
        {
            EstablishTestHost();
        }

        [Fact(DisplayName = "Given invalid token, request returns 403")]
        public async Task CallGetWithInvalidToken()
        {
            // arrange
            var requestPath = TestingServer.BaseAddress + "Drivers/";
            var getRequest = new HttpRequestMessage(HttpMethod.Get, requestPath);

            getRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "invalid_token");
            getRequest.Headers.Host = "localhost";

            // act
            var result = await HttpClient.SendAsync(getRequest);

            // assert
            Assert.Equal(HttpStatusCode.Unauthorized, result.StatusCode);
        }
    }
}
