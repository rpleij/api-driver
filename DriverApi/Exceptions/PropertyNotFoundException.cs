﻿using System;

namespace DriverApi.Exceptions
{
    public class PropertyNotFoundException : Exception
    {
        public PropertyNotFoundException(string propertyName)
            : base($"Could not sort by invalid property: {propertyName}")
        {
        }
    }
}