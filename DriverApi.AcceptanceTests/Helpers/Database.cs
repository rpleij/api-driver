using System.IO;
using System.Text.RegularExpressions;
using Dapper;
using Newtonsoft.Json.Linq;
using SkyDapperExtensions.Services;

namespace DriverApi.AcceptanceTests.Helpers
{
    public class Database
    {
        private static ConnectionService _connectionService;
        private static JObject _appsettingsJson;

        public static void EnsureDropped()
        {
            using (var connection = GetSetupConnectionService().GetConnection())
            {
                var closeCommand = $"IF EXISTS(SELECT Name from sys.Databases " +
                                   $"WHERE Name = '{GetDatabaseName()}') " +
                                   $"ALTER DATABASE {GetDatabaseName()} SET SINGLE_USER " +
                                   $"WITH ROLLBACK IMMEDIATE; ";

                var dropCommand = $"IF EXISTS(SELECT Name from sys.Databases " +
                                  $"WHERE Name = '{GetDatabaseName()}') " +
                                  $"DROP DATABASE {GetDatabaseName()}";
                
                connection.Execute(closeCommand, new DynamicParameters());
                connection.Execute(dropCommand, new DynamicParameters());
            }
        }

        public static void EnsureCreated()
        {
            using (var connection = GetSetupConnectionService().GetConnection())
            {
                var command = string.Format(
                    "CREATE DATABASE {0}",
                    GetDatabaseName());

                connection.Execute(command, new DynamicParameters());
            }
        }

        public static void EnsureSchemaLoaded()
        {
            using (var connection = GetSetupConnectionService().GetConnection())
            {
                connection.Execute(GetConfiguredSqlCommand("Schema"),
                                   new DynamicParameters());
            }
        }

        public static void Seed()
        {
            using (var connection = GetSetupConnectionService().GetConnection())
            {
                connection.Execute(GetConfiguredSqlCommand("LegacySeedData"),
                                   new DynamicParameters(), commandTimeout: 1000);
            }
        }

        public static ConnectionService GetConnectionService()
        {
            return new ConnectionService(GetFullConnectionString());
            //return _connectionService ??
            //    (_connectionService =
            //        new ConnectionService(GetFullConnectionString()));
        }

        // NOTE (jchristie@8thlight.com) This will match and extract
        // the value of the initial catalog from the connection string
        private static string GetDatabaseName()
        {
            var regex = new Regex("(?<=Initial Catalog=)(.*)(?=;user)",
                                  RegexOptions.IgnoreCase);

            return regex.Match(GetFullConnectionString()).
                Groups[0].Value;
        }

        // NOTE (jchristie@8thlight.com) This will match and strip
        // the initial catalog descrption from a connection string
        // so that we don't attempt to default to a database that
        // does not exist on the first test run
        private static string GetConnectionString()
        {
            var regex = new Regex("Initial Catalog=(.*);(?=user)",
                                  RegexOptions.IgnoreCase);
            
            return regex.Replace(GetFullConnectionString(), "");
        }

        private static string GetFullConnectionString()
        {
            return GetAppsettingsJson()
                ["ConnectionStrings"]["LocalDatabaseConnection"]
                .ToString();
        }

        private static ConnectionService GetSetupConnectionService()
        {
            return _connectionService ??
                (_connectionService =
                    new ConnectionService(GetConnectionString()));
        }

        private static JObject GetAppsettingsJson()
        {
            if (_appsettingsJson != null) return _appsettingsJson;
            
            var appsettingsPath = Path.Combine(Directory.GetCurrentDirectory(),
                "appsettings.json");

            var reader = File.OpenText(appsettingsPath);
            _appsettingsJson = JObject.Parse(reader.ReadToEnd());

            return _appsettingsJson;
        }

        private static string GetConfiguredSqlCommand(string fixtureName)
        {
            var rawCommand = File.ReadAllText(
                Path.Combine("Fixture", fixtureName + ".sql"));
            
            return new Regex(
                "API_ACCEPTANCE_TEST_DATABASE_NAME")
                .Replace(rawCommand, GetDatabaseName());
        }
    }
}
