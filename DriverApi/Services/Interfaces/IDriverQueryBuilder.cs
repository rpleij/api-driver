﻿using SkyDataTransferModels;

namespace DriverApi.Services.Interfaces
{
    public interface IDriverQueryBuilder
    {
        SqlWithParameters Build();
        void SetPageSize(int pageSize);
        void SetPageNumber(int pageNumber);
        void SetInactive(bool inactive);
        void SetOrderBy(OrderBy orderBy);
    }
}
