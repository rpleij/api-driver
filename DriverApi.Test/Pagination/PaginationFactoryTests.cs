﻿using DriverApi.Models;
using DriverApi.Exceptions;
using DriverApi.Pagination;
using System.Collections.Generic;
using Xunit;

namespace DriverApi.Tests.Pagination
{
    public class PaginationFactoryTests
    {
        [Fact]
        public void CreateWithPageNumberReturnsPaginationResponse()
        {
            // arrange
            const int pageNumber = 1;
            const string baseUrl = "http://testserver.com/Drivers";
            var expectedList = new List<Driver>
            {
                new Driver
                {
                    UniqueId = 1,
                    TotalSqlRowCount = 1
                }
            };
            IBuildsPaginationResponses<Driver> factory = new PaginationResponseFactory<Driver>(baseUrl);

            // act
            var result = factory.Create(expectedList, pageNumber,1);

            // assert
            Assert.Equal(1, result.MetaResponseData.TotalPages);
            Assert.Equal(null, result.LinksMetaData.Prev);
            Assert.Equal(null, result.LinksMetaData.Next);
            Assert.Equal($"{baseUrl}?pageNumber=1", result.LinksMetaData.Last);
            Assert.Equal($"{baseUrl}?pageNumber=1", result.LinksMetaData.Self);
            Assert.Equal($"{baseUrl}?pageNumber=1", result.LinksMetaData.First);
            Assert.Equal(expectedList, result.Data);
        }

        [Fact(DisplayName = "Given page number greater than the number of pages, throw exception")]
        public void PageNumberGreaterThanNumberOfPagesThrowsException()
        {
            // arrange
            const int pageNumber = 50;
            const string baseUrl = "http://testserver.com/Drivers";
            var DriverList = new List<Driver>();
            for (var i = 1; i < 799; i++)
            {
                DriverList.Add(new Driver { UniqueId = 1, TotalSqlRowCount = 799});
            }

            IBuildsPaginationResponses<Driver> factory = new PaginationResponseFactory<Driver>(baseUrl);

            // assert
            Assert.Throws<DriverPageConstructionException>(() => factory.Create(DriverList, pageNumber, 799));
        }

        [Fact(DisplayName = "Given page number 0, thow exception")]
        public void PageNumber0ThrowsException()
        {
            // arrange
            const int pageNumber = 0;
            const string baseUrl = "http://testserver.com/Drivers";
            var DriverList = new List<Driver>();
            for (var i = 1; i < 799; i++)
            {
                DriverList.Add(new Driver { UniqueId = 1, TotalSqlRowCount = 799});
            }

            IBuildsPaginationResponses<Driver> factory = new PaginationResponseFactory<Driver>(baseUrl);

            // assert
            Assert.Throws<DriverPageConstructionException>(() => factory.Create(DriverList, pageNumber, 799));
        }

        [Fact(DisplayName = "Given a negative page number, thow exception")]
        public void PageNumberNegative1ThrowsException()
        {
            // arrange
            const int pageNumber = -1;
            const string baseUrl = "http://testserver.com/Drivers";
            var DriverList = new List<Driver>();
            for (var i = 1; i < 799; i++)
            {
                DriverList.Add(new Driver { UniqueId = 1, TotalSqlRowCount = 799});
            }

            IBuildsPaginationResponses<Driver> factory = new PaginationResponseFactory<Driver>(baseUrl);

            // assert
            Assert.Throws<DriverPageConstructionException>(() => factory.Create(DriverList, pageNumber, 799));
        }
    }
}
