using System;
using System.Net.Http;
using Dapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using SkyDapperExtensions.Services.Interfaces;

namespace DriverApi.AcceptanceTests.Helpers
{
    public class TestBase : IDisposable
    {
        private TestServer _testingServer;
        public static string TransactionPrefix = "AcceptanceTestTransaction";

        protected readonly string DatabaseId = "AcptDelTic";
        protected readonly IConnection Connection;
        protected WebHostBuilder HostBuilder { get; }
        protected TestServer TestingServer
        {
            get
            {
                if (_testingServer == null)
                {
                    throw new Exception("EstablishTestHost has not yet been called. TestingServer is null.");
                }
                return _testingServer;
            }
            private set
            {
                _testingServer = value;
            }
        }
        protected HttpClient HttpClient { get; private set; }

        public TestBase() : this(new WebHostBuilder().UseStartup<AuthorizationAllowStartup>())
        { }

        public TestBase(IWebHostBuilder builder)
        {
            Connection = Database.GetConnectionService()
                .GetConnection();
            Connection.OpenConnection();

            BeginTransaction();
            HostBuilder = (WebHostBuilder)builder;
        }

        protected void EstablishTestHost()
        {
            if (HttpClient != null)
                throw new TestHostAlreadyCreated();

            TestingServer = new TestServer(HostBuilder);
            HttpClient = TestingServer.CreateClient();
        }

        public virtual void Dispose()
        {
            Connection.Dispose();
        }

        private void BeginTransaction()
        {
            Connection.Execute(
                "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" +
                $"BEGIN TRANSACTION {GetNewTransactionName()}",
                new DynamicParameters());
        }

        private static string GetNewTransactionName()
        {
            return string.Format(
                "{0}_{1}",
                TransactionPrefix,
                new Random().Next(0, 10000));
        }
    }
}
