﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SkyDapperExtensions.Services.Interfaces;
using SkyDapperExtensions.Services;
using Microsoft.Extensions.DependencyInjection.Extensions;
using DriverApi.Services.Interfaces;
using DriverApi.Services;
using DriverApi.Pagination;
using DriverApi.Models;
using Microsoft.AspNetCore.HttpOverrides;
using Swashbuckle.Swagger.Model;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Microsoft.Extensions.Options;
using IdentityServer4.AccessTokenValidation;
using AgvanceSkyModels;

namespace DriverApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            //string connectionString = Configuration.GetConnectionString("LocalSqlServerConnection");
            var connectionString = Configuration.GetConnectionString("SqlServerConnection");
            var baseDriverTicketUrl = Configuration.GetSection("InternalRequests").GetValue(typeof(string), "DriverControllerUrl").ToString();

            // Add framework services.
            services.AddMvc();
            services.AddScoped<IConnectionService, ConnectionService>(x => new ConnectionService(connectionString));
            services.TryAddTransient<IConnectionQueryBuilder, ConnectionQueryBuilder>();
            services.TryAddTransient<IDriverQueryBuilder, DriverQueryBuilder>();
            services.TryAddTransient<IDriverDataService, DriverDataService>();
            services.TryAddTransient<IEncryptionService, EncryptionService>();
            services.TryAddScoped<IAgvanceDatabaseConnectionService, AgvanceDatabaseConnectionService>();
            services.TryAddTransient<IBuildsPaginationResponses<Driver>>(x => new PaginationResponseFactory<Driver>(baseDriverTicketUrl));
            services.AddSwaggerGen();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedProto;
            });
            services.Configure<IdentityServer>(Configuration.GetSection("IdentityServer"));
            services.ConfigureSwaggerGen(options =>
            {
                options?.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Agvance Sky Driver API",
                    Description = "REST API that provides Driver Information",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Agvance SKY Team 3", Email = "agroskycos@agvance.net", Url = "http://www.agvance.net" },
                    License = new License { Name = "Use under LICX", Url = "http://www.agvance.net" }
                });

                options?.AddSecurityDefinition("yourapi_oauth2", new OAuth2Scheme
                {
                    Description = "OAuth2 client credentials flow",
                    Type = "oauth2",
                    Flow = "clientcredentials",
                    AuthorizationUrl = Configuration["OpenId:authority"],
                    TokenUrl = Configuration["OpenId:authority"] + "/connect/token",
                    Scopes = new Dictionary<string, string> { { "yourapi", "your api resources" } }
                });
                options?.OperationFilter<ApplyOAuth2Security>();
                options?.DocumentFilter<ApplyOAuth2Security>();

#if DEBUG
                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "DriverApi.xml");
                options?.IncludeXmlComments(xmlPath);
#endif
            });
            
         }
        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Implementation of the IApplicationBuilder</param>
        /// <param name="env">Implementation of the IHostingEnvironment</param>
        /// <param name="loggerFactory">Implemenation of the ILoggerFactory</param>
        /// <param name="identityconfig">Implemenation of the IOptions that contains an IdentityServer model</param>
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IOptions<IdentityServer> identityconfig)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = identityconfig.Value.Authority,
                SupportedTokens = SupportedTokens.Jwt,
                AuthenticationScheme = "Bearer",
                RequireHttpsMetadata = !identityconfig.Value.AllowInsecure ?? true,
                ApiName = "skydentity",
                ApiSecret = "skydentity-api-secret",
                AllowedScopes = new List<string>
                {
                    "skydentity"
                }
            });

            app.UseCors("CorsPolicy");
            app.UseMvc();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUi();
        }
    }
}
