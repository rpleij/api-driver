﻿using DriverApi.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DriverApi.Models;
using SkyDataTransferModels;
using SkyDapperExtensions.Services.Interfaces;

namespace DriverApi.Services
{
    public class DriverDataService : IDriverDataService
    {
        private readonly IAgvanceDatabaseConnectionService _connectionService;
        private readonly IDriverQueryBuilder _driverQueryBuilder;

        public DriverDataService(IDriverQueryBuilder driverQueryBuilder, IAgvanceDatabaseConnectionService connectionService)
        {
            _connectionService = connectionService;
            _driverQueryBuilder = driverQueryBuilder;
        }

        public Driver Fetch() => FetchList().FirstOrDefault();

        public async Task<Driver> FetchAsync() => (await FetchListAsync())?.FirstOrDefault();

        public List<Driver> FetchList()
        {
            using (var connection = _connectionService.GetConnection())
            {
                var query = _driverQueryBuilder.Build();
                var driverList = connection.Query<Driver>(query.Sql, query.SqlParameters)?.ToList();

                return driverList;
            }
        }

        public async Task<List<Driver>> FetchListAsync()
        {
            using (var connection = _connectionService.GetConnection())
            {
                var query = _driverQueryBuilder.Build();
                var driverList = (await connection.QueryAsync<Driver>(query.Sql, query.SqlParameters))?.ToList();

                return driverList;
            }
        }

        public void SetInactive(bool inactive)
        {
            _driverQueryBuilder.SetInactive(inactive);
        }

        public void SetOrderBy(OrderBy orderBy)
        {
            _driverQueryBuilder.SetOrderBy(orderBy);
        }

        public void SetPageNumber(int pageNumber)
        {
            _driverQueryBuilder.SetPageNumber(pageNumber);
        }

        public void SetPageSize(int pageSize)
        {
            _driverQueryBuilder.SetPageSize(pageSize);
        }

    }
}
