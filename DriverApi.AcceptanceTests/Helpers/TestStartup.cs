using AgvanceSkyModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DriverApi.AcceptanceTests.Helpers
{
    internal class AuthorizationAllowStartup : Startup
    {
        public AuthorizationAllowStartup(IHostingEnvironment env) : base(env) {}

        public override void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              ILoggerFactory loggerFactory,
                              IOptions<IdentityServer> identityConfig)
        {
            app.UseMiddleware<BypassAuthMiddleware>();
            base.Configure(app, env, loggerFactory, identityConfig);
        }
    }
}