﻿namespace DriverApi.Pagination
{
    /// <summary>
    /// Metadata about the paged response
    /// </summary>
    public class MetaResponseData
    {
        /// <summary>
        /// Indicates the total number of pages available for this data
        /// </summary>
        public int TotalPages { get; set; }
    }
}