USE [API_ACCEPTANCE_TEST_DATABASE_NAME]

SET XACT_ABORT ON;

BEGIN TRANSACTION AcceptanceTestSchema;

/****** Object:  Table [dbo].[agdriver]    Script Date: 08/25/2017 3:08:48 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[agdriver](
	[UniqueID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [nvarchar](6) NOT NULL,
	[fname] [nvarchar](20) NULL,
	[lname] [nvarchar](40) NULL,
	[careof] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[city] [nvarchar](25) NULL,
	[state] [nvarchar](2) NULL,
	[zip] [nvarchar](10) NULL,
	[phone] [nvarchar](15) NULL,
	[phone2] [nvarchar](15) NULL,
	[birthday] [datetime] NULL,
	[applic] [nvarchar](80) NULL,
	[appexp] [datetime] NULL,
	[lastdrug] [datetime] NULL,
	[lastphy] [datetime] NULL,
	[cdllic] [nvarchar](20) NULL,
	[cdlexp] [datetime] NULL,
	[comments] [ntext] NULL,
	[DriverType] [nvarchar](20) NULL,
	[EmailAddr] [nvarchar](50) NULL,
	[Inactive] [bit] NULL,
	[LocationID] [nvarchar](6) NULL,
	[UserID] [nvarchar](3) NULL,
	[DivisionType] [nvarchar](20) NOT NULL,
 CONSTRAINT [aaaaaagdriver_PK] PRIMARY KEY NONCLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_agdriver] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[agdriver] ADD  DEFAULT ('') FOR [DivisionType]

COMMIT TRANSACTION AcceptanceTestSchema;
