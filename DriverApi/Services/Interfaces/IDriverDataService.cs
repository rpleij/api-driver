﻿using DriverApi.Models;
using System.Collections.Generic;
using SkyDataTransferModels;
using System.Threading.Tasks;

namespace DriverApi.Services.Interfaces
{
    public interface IDriverDataService
    {
        List<Driver> FetchList();
        Driver Fetch();
        Task<List<Driver>> FetchListAsync();
        Task<Driver> FetchAsync();
        void SetPageSize(int pageSize);
        void SetPageNumber(int pageNumber);
        void SetInactive(bool inactive);
        void SetOrderBy(OrderBy orderBy);
    }
}
