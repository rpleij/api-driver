﻿using DriverApi.Services.Interfaces;
using System;
using SkyDataTransferModels;
using System.Text;
using Dapper;
using DriverApi.Models;
using System.Reflection;
using System.Collections.Generic;
using DriverApi.Exceptions;
using System.Linq;

namespace DriverApi.Services
{
    /// <summary>
    /// Query builder for fetching drivers
    /// </summary>
    public class DriverQueryBuilder : IDriverQueryBuilder
    {
        private bool? _inactive = null;
        private int? _pageSize = 20;
        private int? _pageNumber = 1;
        private OrderBy _orderBy;

        private static string GetDriverSql()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("SELECT")
                         .AppendLine("  TotalSqlRowCount = COUNT(*) OVER()")
                         .AppendLine(", agdriver.UniqueID")
                         .AppendLine(", agdriver.ID")
                         .AppendLine(", agdriver.fname")
                         .AppendLine(", agdriver.lname")
                         .AppendLine(", agdriver.EmailAddr")
                         .AppendLine(", agdriver.DriverType")
                         .AppendLine(", agdriver.DivisionType")
                         .AppendLine(", agdriver.Inactive")
                         .AppendLine("FROM agdriver")
                         .AppendLine("WHERE 1 = 1");
            return stringBuilder.ToString();
        }

        private string AddInactiveToWhereClause(DynamicParameters parameters)
        {
            if (_inactive == null) return string.Empty;
            parameters.Add(nameof(Driver.IsInactive), _inactive);
            return $"AND agdriver.Inactive = @{nameof(Driver.IsInactive)}";
        }

        private string AddOrderBy(DynamicParameters parameters)
        {
            var instance = new Driver();

            var attributes = instance.GetType().GetProperties();

            var dbColumnNameDictionary = attributes.Where(
                    attribute => attribute?.GetCustomAttribute(typeof(CustomAttributes.ColumnAttribute)) != null)
                .ToDictionary(attribute => attribute.Name,
                    attribute =>
                        ((CustomAttributes.ColumnAttribute)
                            (attribute.GetCustomAttribute(typeof(CustomAttributes.ColumnAttribute)))).Name);

            var propertyTableDictionary = attributes.Where(
                    attribute => attribute?.GetCustomAttribute(typeof(CustomAttributes.TableAttribute)) != null)
                .ToDictionary(attribute => attribute.Name,
                    attribute =>
                        ((CustomAttributes.TableAttribute)
                            (attribute.GetCustomAttribute(typeof(CustomAttributes.TableAttribute)))).Name);



            if (_orderBy == null) return "ORDER BY Delivery.DtNum, Delivery.Location, Delivery.DtDate";

            string returnValue;

            try
            {
                var sortDirection = _orderBy.sortDirection == SortDirectionEnum.ASC ? "ASC" : "DESC";

                // ORDER BY does ot allow parameters in SQL. So the names are put into the string here. Because we the table
                // name and column name come from the reflection of the Driver object no SQL injection is possible.

                returnValue = $"ORDER BY [{propertyTableDictionary[_orderBy.propertyName]}].[{dbColumnNameDictionary[_orderBy.propertyName]}] {sortDirection}";
            }
            catch (KeyNotFoundException)
            {
                throw new PropertyNotFoundException(_orderBy.propertyName);
            }

            return returnValue;
        }

        private string AddOffSetRows(DynamicParameters parameters)
        {
            if (_pageNumber == null || _pageSize == null) return "OFFSET 0 ROWS";
            var placeHolder = "OffSet";
            parameters.Add(placeHolder, ((_pageNumber - 1) * _pageSize));
            return $"OFFSET @{placeHolder} ROWS";
        }

        private string AddFetchOnly(DynamicParameters parameters)
        {
            if (_pageSize == null) return "FETCH NEXT 20 ROWS ONLY";
            var placeHolder = "FetchNextRowsOnly";
            parameters.Add(placeHolder, _pageSize);
            return $"FETCH NEXT @{placeHolder} ROWS ONLY";
        }

        /// <summary>
        /// Creats the approriate SqlWithParameters for fetching drivers
        /// </summary>
        /// <returns></returns>
        public SqlWithParameters Build()
        {
            var stringBuilder = new StringBuilder();
            var parameters = new DynamicParameters();
            stringBuilder.AppendLine(GetDriverSql())
                         .AppendLine(AddInactiveToWhereClause(parameters))
                         .AppendLine(AddOrderBy(parameters))
                         .AppendLine(AddOffSetRows(parameters))
                         .AppendLine(AddFetchOnly(parameters));
            return new SqlWithParameters
            {
                Sql = stringBuilder.ToString(),
                SqlParameters = parameters
            };
        }

        /// <summary>
        /// Set the inactive where clause
        /// </summary>
        /// <param name="inactive"></param>
        public void SetInactive(bool inactive) => _inactive = inactive;

        /// <summary>
        /// Sets the Order By for the query
        /// </summary>
        /// <param name="orderBy"></param>
        public void SetOrderBy(OrderBy orderBy) => _orderBy = orderBy;

        /// <summary>
        /// Setst the Page number for the query to be returned
        /// </summary>
        /// <param name="pageNumber"></param>
        public void SetPageNumber(int pageNumber) => _pageNumber = pageNumber;

        /// <summary>
        /// Sets the Page Size for the query to be returned
        /// </summary>
        /// <param name="pageSize"></param>
        public void SetPageSize(int pageSize) => _pageSize = pageSize;
    }
}
