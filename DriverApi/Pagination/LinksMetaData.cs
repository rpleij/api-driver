﻿namespace DriverApi.Pagination
{
    /// <summary>
    /// Metadata for paging hyperlinks
    /// </summary>
    public class LinksMetaData
    {
        /// <summary>
        /// Hyperlink for previous page of data
        /// </summary>
        public string Prev { get; set; }

        /// <summary>
        /// Hyperlink for next page of data
        /// </summary>
        public string Next { get; set; }

        /// <summary>
        /// Hyperlink for last page of data
        /// </summary>
        public string Last { get; set; }

        /// <summary>
        /// Hyperlink for this page of data
        /// </summary>
        public string Self { get; set; }

        /// <summary>
        /// Hyperlink for first page of data
        /// </summary>
        public string First { get; set; }
    }
}