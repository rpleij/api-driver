﻿using System.Collections.Generic;

namespace DriverApi.Pagination
{
    /// <summary>
    /// A paginated list of T
    /// </summary>
    /// <typeparam name="T">Type of object to be returned in the list</typeparam>
    public class PaginationResponse<T>
    {
        /// <summary>
        /// Metadata about the list of T
        /// </summary>
        public MetaResponseData MetaResponseData { get; set; }

        /// <summary>
        /// Hyperlink metadata about the list of T
        /// </summary>
        public LinksMetaData LinksMetaData { get; set; }

        /// <summary>
        /// Limited list of T
        /// </summary>
        public IEnumerable<T> Data { get; set; }
    }
}