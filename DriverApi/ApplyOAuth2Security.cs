﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;

namespace DriverApi
{
    public class ApplyOAuth2Security : IDocumentFilter, IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var authorizationRequired = context.ApiDescription.GetControllerAttributes().Any(a => a is AuthorizeAttribute);

            if (!authorizationRequired)
            {
                authorizationRequired = context.ApiDescription.GetActionAttributes().Any(a => a is AuthorizeAttribute);
            }

            if (!context.ApiDescription.ActionDescriptor.FilterDescriptors.Select(f => f.Filter).Any(f => f is AuthorizeFilter) || !authorizationRequired)
            {
                return;
            }

            if (operation.Parameters == null)
            {
                operation.Parameters = new List<IParameter>();
            }

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = "Authorization",
                In = "header",
                Description = "JWT security token obtained from Identity Server.",
                Required = true,
                Type = "string"
            });
        }

        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Security = swaggerDoc.SecurityDefinitions.Select(
                    securityDefinition => new Dictionary<string, IEnumerable<string>>
                    {
                        {securityDefinition.Key, new[] {"yourapi"}}
                    })
                .Cast<IDictionary<string, IEnumerable<string>>>()
                .ToList();
        }
    }
}
