﻿using System;
using System.Collections.Generic;
using CustomAttributes;
using Newtonsoft.Json;

namespace DriverApi.Models
{
    public class Driver
    {
        /// <summary>
        ///Unique id of the record
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "UniqueID")]
        [IsPrimaryKey]
        public int UniqueId { get; set; }
        /// <summary>
        /// Id of the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "ID")]
        public string Id { get; set; }

        /// <summary>
        /// Firstname of the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "fname")]
        public string FirstName { get; set; }

        /// <summary>
        /// Lastname of the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "lname")]
        public string LastName { get; set; }

        /// <summary>
        /// Emailaddress associated with the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "EmailAddr")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Driver type associated wuth the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "DriverType")]
        public string DriverType { get; set; }

        /// <summary>
        /// Division type associated wuth the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "DivisionType")]
        public string DivisionType { get; set; }

        /// <summary>
        /// Division type associated wuth the driver
        /// </summary>
        [Table(Name = "agdriver")]
        [Column(Name = "Inactive")]
        public bool IsInactive { get; set; }

        [JsonIgnore]
        [Column(Name = "TotalSqlRowCount")]
        public int TotalSqlRowCount { get; set; }
    }
}
