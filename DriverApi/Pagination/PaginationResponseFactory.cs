﻿using DriverApi.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DriverApi.Pagination
{
    /// <summary>
    /// 
    /// </summary>
    public class PaginationResponseFactory<T> : IBuildsPaginationResponses<T>
    {
        private readonly string _baseUrl;
        private const int PageSize = 20;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseUrl"></param>
        public PaginationResponseFactory(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Driver"></param>
        /// <param name="pageNumber"></param>
        /// <param name="numberOfRecords"></param>
        /// <returns></returns>
        public PaginationResponse<T> Create(List<T> Driver, int pageNumber, int numberOfRecords)
        {
            int totalPages = (int)Math.Ceiling((double)numberOfRecords / PageSize);
            var nextPage = totalPages > pageNumber ? pageNumber + 1 : (int?)null;
            var prevPage = pageNumber > 1 ? pageNumber - 1 : (int?)null;

            var requiredTicketStart = PageSize * (pageNumber - 1);

            var response = new PaginationResponse<T>();
            if (pageNumber > totalPages || pageNumber < 1)
            {
                throw new DriverPageConstructionException($"Total Number of pages: {totalPages}{Environment.NewLine}" +
                                                              $"Requested page: {pageNumber}{Environment.NewLine}" +
                                                              $"Total number of Records: {Driver.Count}");
            }

            response.Data = Driver;
            response.LinksMetaData = new LinksMetaData
            {
                First = $"{_baseUrl}?pageNumber=1",
                Self = $"{_baseUrl}?pageNumber={pageNumber}",
                Next = nextPage != null ? $"{_baseUrl}?pageNumber={nextPage}" : null,
                Prev = prevPage != null ? $"{_baseUrl}?pageNumber={prevPage}" : null,
                Last = $"{_baseUrl}?pageNumber={totalPages}",
            };
            response.MetaResponseData = new MetaResponseData { TotalPages = totalPages };

            return response;
        }
    }
}